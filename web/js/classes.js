// Classes
function AjaxUploaderForm() {
    var url;
    var selectorInput;

    this.setUrl = function(chosenUrl) {url = chosenUrl};
    this.setSelectorInput = function(selector) {selectorInput = selector};
    this.makeAjaxUploadForm = function() {
        $(selectorInput).change(function () {
            var file = this.files[0];
            if (file.type != 'video/mp4' && file.type != 'video/quicktime') {
                alert('Incorrect input format file. Supported formats: mp4 or mov');
                this.parentElement.parentElement.reset();
                return;
            }
            sendFile(this.files[0]);
        });
        function sendFile(file) {
            $.ajax({
                type: 'post',
                url: Routing.generate(url, {name: file.name}),
                data: file,
                success: function () {
                    // do something
                },
                xhrFields: {
                    // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
                    onprogress: function (progress) {
                        // calculate upload progress
                        var percentage = Math.floor((progress.total / progress.totalSize) * 100);
                        // log upload progress to console
                        console.log('progress', percentage);
                        if (percentage === 100) {
                            console.log('DONE!');
                        }
                    }
                },
                processData: false,
                contentType: file.type
            });
        }
    }
}

function setActiveClass(current, allElements) {
    allElements.removeClass('active');
    current.addClass('active');
}

function SortableManager(chosenSelector) {
    var selector = chosenSelector;
    this.makeSortable = function() {
        $(selector).sortable();
        $(selector).disableSelection();
        $(selector).on("sortupdate", function(event, ui) {
            var elementsWithPosition = positionManager.getPositionsElements();
            var url = Routing.generate('ajax_position_update', {id: currentPlaylist});
            $.post(url, JSON.stringify(elementsWithPosition));
        });
    };
}

function PositionManager(chosenSelector) {
    var selector = chosenSelector;
    this.getPositionsElements = function() {
        var elements = $(selector);
        var lastPosition = 0;
        var elementsWithPosition = [];
        elements.each(function (i, e) {
            var minElement = getElementWithMinOffset(elements);
            var element = {};
            lastPosition += 1;
            element.id = $(e).attr('id');
            element.position = lastPosition;
            elementsWithPosition.push(element);
            elements = dropElementFromCollection(elements, minElement);
        });

        return elementsWithPosition;
    };
    function getElementWithMinOffset(elements) {
        var minElement  = elements.eq(0);
        for (var i = 0; i < elements.length; i++) {
            var offsetTop = elements.eq(i).offset().top;
            if (offsetTop < minElement.offset().top) {
                minElement = elements.eq(i);
            }
        }

        return minElement;
    }
    function dropElementFromCollection(elements, element) {
        var elements = elements.filter(function(index) {
            return element.attr('id') != elements.eq(index).attr('id');
        });

        return elements;
    }
}