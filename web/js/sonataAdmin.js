$(function() {
    var modal = $('.modal');
    var myPlayer;
    $('.sonata-ba-list-field.sonata-ba-list-field-text .video').on('click', function() {
        var path = $(this).attr('path');
        var format = path.match(/.{3}$/);
        if (format == 'mp4' || format == 'm4v') {
            var mimeType = 'video/mp4';
        } else if (format == 'mov') {
            var mimeType = 'video/quicktime';
        }
        modal.modal('show');
        videojs("video_block").ready(function(){
            myPlayer = this;
            myPlayer.src({ type: mimeType, src: path });
            myPlayer.play();
        });
        $('#video_block').css({height: '100%', width: '100%'});
    });
    modal.on('hide.bs.modal', function () {
        videojs("video_block").ready(function(){
            myPlayer.pause();
        });
    })
});

