<?php

namespace App\ActiveScreenBundle\Twig;

class SmartMirrorExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('name_from_path', array($this, 'getNameFromPath')),
            new \Twig_SimpleFilter('file_size', array($this, 'getFileSize')),
        );
    }

    public function getNameFromPath($path)
    {
        preg_match('/\/([\w\d\\s_\+\-\[\]0-9]+)\.([\w\d]+)/', $path, $name);

        return $name[1];
    }

    public function getFileSize($path)
    {
        @filesize($path) ? $size = filesize($path) : $size = 0;

        return $size;
    }




    public function getName()
    {
        return 'sm_extension';
    }
}
