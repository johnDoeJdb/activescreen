<?php

namespace App\ActiveScreenBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ActiveScreenBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
