<?php

namespace App\ActiveScreenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Horoscope
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\ActiveScreenBundle\Repository\HoroscopeRepository")
 */
class Horoscope
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="sign", type="string", length=255)
     */
    private $sign;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @return Horoscope
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get data
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set sign
     *
     * @param string $sign
     * @return Horoscope
     */
    public function setSign($sign)
    {
        $this->sign = $sign;
    
        return $this;
    }

    /**
     * Get sign
     *
     * @return string 
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Horoscope
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}