<?php

namespace App\ActiveScreenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\ActiveScreenBundle\Entity\ItemRepository")
 */
class Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Video")
     */
    private $video;

    /**
     * @ORM\Column(name="horoscope", type="boolean", nullable=true)
     */
    private $horoscope;

    /**
     * @ORM\Column(name="weather", type="boolean", nullable=true)
     */
    private $weather;

    /**
     * @ORM\Column(name="template", type="boolean", nullable=true)
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="Playlist", inversedBy="item")
     */
    private $playlist;

    public function __toString()
    {
        if ($this->horoscope) {
            return 'horoscope';
        } elseif ($this->weather) {
            return 'weather';
        } elseif ($this->video) {
            return $this->getVideo()->getFile();
        } else {
            return 'item';
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Item
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set video
     *
     * @param \App\ActiveScreenBundle\Entity\Video $video
     * @return Item
     */
    public function setVideo(\App\ActiveScreenBundle\Entity\Video $video = null)
    {
        $this->video = $video;
    
        return $this;
    }

    /**
     * Get video
     *
     * @return \App\ActiveScreenBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set playlist
     *
     * @param \App\ActiveScreenBundle\Entity\Playlist $playlist
     * @return Item
     */
    public function setPlaylist(\App\ActiveScreenBundle\Entity\Playlist $playlist = null)
    {
        $this->playlist = $playlist;
    
        return $this;
    }

    /**
     * Get playlist
     *
     * @return \App\ActiveScreenBundle\Entity\Playlist 
     */
    public function getPlaylist()
    {
        return $this->playlist;
    }

    /**
     * Set horoscope
     *
     * @param boolean $horoscope
     * @return Item
     */
    public function setHoroscope($horoscope)
    {
        $this->horoscope = $horoscope;
    
        return $this;
    }

    /**
     * Get horoscope
     *
     * @return boolean 
     */
    public function getHoroscope()
    {
        return $this->horoscope;
    }

    /**
     * Set weather
     *
     * @param boolean $weather
     * @return Item
     */
    public function setWeather($weather)
    {
        $this->weather = $weather;
    
        return $this;
    }

    /**
     * Get weather
     *
     * @return boolean 
     */
    public function getWeather()
    {
        return $this->weather;
    }

    /**
     * Set template
     *
     * @param boolean $template
     * @return Item
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    
        return $this;
    }

    /**
     * Get template
     *
     * @return boolean 
     */
    public function getTemplate()
    {
        return $this->template;
    }
}