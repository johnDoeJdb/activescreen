<?php

namespace App\ActiveScreenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Playlist
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\ActiveScreenBundle\Repository\PlaylistRepository")
 */
class Playlist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="playlist", cascade={"all"}, orphanRemoval=true)
     */
    private $item;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="playlist", cascade={"all"}, orphanRemoval=true)
     */
    private $user;

    /**
     * Constructor
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->item = new \Doctrine\Common\Collections\ArrayCollection();
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Playlist
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add item
     *
     * @param \App\ActiveScreenBundle\Entity\Item $item
     * @return Playlist
     */
    public function addItem(\App\ActiveScreenBundle\Entity\Item $item)
    {
        $this->item[] = $item;
    
        return $this;
    }

    /**
     * Remove item
     *
     * @param \App\ActiveScreenBundle\Entity\Item $item
     */
    public function removeItem(\App\ActiveScreenBundle\Entity\Item $item)
    {
        $this->item->removeElement($item);
    }

    /**
     * Get item
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Add user
     *
     * @param \App\ActiveScreenBundle\Entity\User $user
     * @return Playlist
     */
    public function addUser(\App\ActiveScreenBundle\Entity\User $user)
    {
        $this->user[] = $user;
    
        return $this;
    }

    /**
     * Remove user
     *
     * @param \App\ActiveScreenBundle\Entity\User $user
     */
    public function removeUser(\App\ActiveScreenBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUser()
    {
        return $this->user;
    }
}