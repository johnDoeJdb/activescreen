<?php

namespace App\ActiveScreenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Video
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Video
{
    const VIDEO_FOLDER = '/media/video';
    const THUMBNAIL_FOLDER = '/media/thumbnail';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255)
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=255)
     */
    private $thumbnail;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="video")
     */
    private $item;

    /**
     * Unmapped property to handle file uploads
     */
    private $uploadFile;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setUploadFile(UploadedFile $file = null)
    {
        $this->uploadFile = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getUploadFile()
    {
        return $this->uploadFile;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getUploadFile()) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and target filename as params
        $this->getUploadFile()->move(
            '.'.$this::VIDEO_FOLDER,
            $this->getUploadFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->file = $this::VIDEO_FOLDER.'/'.$this->getUploadFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setUploadFile(null);
    }

    /**
     * Lifecycle callback to upload the file to the server
     */
    public function lifecycleFileUpload() {
        $this->upload();
    }

    public function __toString()
    {
        return $this->file;
    }

    public function __construct()
    {
        $this->item = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add item
     *
     * @param \App\ActiveScreenBundle\Entity\Item $item
     * @return Video
     */
    public function addItem(\App\ActiveScreenBundle\Entity\Item $item)
    {
        $this->item[] = $item;
    
        return $this;
    }

    /**
     * Remove item
     *
     * @param \App\ActiveScreenBundle\Entity\Item $item
     */
    public function removeItem(\App\ActiveScreenBundle\Entity\Item $item)
    {
        $this->item->removeElement($item);
    }

    /**
     * Get item
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItem()
    {
        return $this->item;
    }

    public function fetchFormat()
    {
        preg_match('/.{3}$/', $this->file, $format);

        return $this->format;
    }

    public function fetchName()
    {
        preg_match('/.{3}$/', $this->file, $format);

        return $this->format;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return Video
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    
        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Video
     */
    public function setFile($file)
    {
        $this->file = $file;
    
        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }
}