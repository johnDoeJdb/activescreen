<?php

namespace App\ActiveScreenBundle\Manager;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use App\ActiveScreenBundle\Entity\User;

class AuthenticationManager
{

    private $container;

    function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function checkUserCredentials($user, $password)
    {
        $credentials = false;
        if ($user && get_class($user) == 'App\ActiveScreenBundle\Entity\User') {
            $encoder_service = $this->container->get('security.encoder_factory');
            $encoder = $encoder_service->getEncoder($user);
            $encoded_pass = $encoder->encodePassword($password, $user->getSalt());
            $credentials = $encoded_pass == $user->getPassword();
        }

        return $credentials;
    }

    public function getResponseError()
    {
        $error['error'] = 'Invalid user or password';
        $response = new JsonResponse($error);

        return $response;
    }
}
