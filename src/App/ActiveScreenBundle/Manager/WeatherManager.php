<?php

namespace App\ActiveScreenBundle\Manager;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\SimpleXMLElement;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

class WeatherManager
{
    const YAHOO_WEATHER_API_HOST = 'http://weather.yahooapis.com/forecastrss';

    private $container;
    private $weather;

    function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getForecast($latitude, $longitude)
    {
        $this->getWeather($latitude, $longitude);
        $xml = new SimpleXMLElement($this->weather);
        $xml->registerXPathNamespace('yweather', 'http://xml.weather.yahoo.com/ns/rss/1.0');
        $item = $xml->channel->item;
        $location = $xml->channel->xpath('yweather:location')[0]->attributes();
        $week = $item->xpath('yweather:forecast');
        $forecast = [];
        foreach ($location as $key => $value) {
            if ($key == 'city') {
                $city = $value;
            }
        }
        foreach ($week as $day) {
            $item = [];
            $item['day'] = (string) $day->attributes()->day;
            $item['date'] = (string) $day->attributes()->date;
            $item['low'] = $this->convertFahrenheitToCelsius((string) $day->attributes()->low);
            $item['high'] = $this->convertFahrenheitToCelsius((string) $day->attributes()->high);
            $item['state'] = $this->getState((string) $day->attributes()->code);
            $item['city'] = (string) $city;
            $forecast[] = $item;
        }

        return $forecast;
    }

    private function convertFahrenheitToCelsius($fahrenheit) {
        return $celsius = (($fahrenheit-32)/2);
    }

    private function getWeather($latitude, $longtitude)
    {
        $woeid = $this->getWOEID($latitude, $longtitude);
        $weather = @file_get_contents($this::YAHOO_WEATHER_API_HOST.'?w='.$woeid);
        $this->weather = $weather;
    }

    private function getWOEID($latitude, $longtitude)
    {
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "http://query.yahooapis.com/v1/public/yql?q=select+*+from+geo.places+where+text%3D%22{$latitude}+{$longtitude}%22",
            CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.14 Safari/537.36'
        ));
        // Send the request & save response to $resp
        $response = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        $crawler = new Crawler($response);
        $woeid = $crawler->filter('woeid')->text();

        return $woeid;
    }

    private function getState($code)
    {
        $tornado = [0];
        $tropicalStorm = [1, 2, 19, 23, 24];
        $severeThunderstorms = [3, 4, 37, 45, 47];
        $mixedRainSnow = [5, 6, 7, 8, 10, 18];
        $drizzle = [9, 11, 12];
        $showers = [11, 12];
        $snowFlurries = [13, 14, 15, 16, 41, 42, 46];
        $hail = [17];
        $foggy = [20, 21, 22];
        $cold = [25];
        $cloudy = [26];
        $mostlyCloudy = [27, 28];
        $partlyCloudy = [29, 30, 44];
        $clear = [31, 32,33, 34, 36];
        $mixedRainHail = [35];
        $scatteredThunderstorms = [38, 39, 40];

        if (in_array($code, $tornado)) {
            $state = 'tornado';
        } elseif (in_array($code, $tropicalStorm)) {
            $state = 'tropical storm';
        } elseif (in_array($code, $severeThunderstorms)) {
            $state = 'severe thunderstorms';
        } elseif (in_array($code, $mixedRainSnow)) {
            $state = 'mixed rain and snow';
        } elseif (in_array($code, $drizzle)) {
            $state = 'drizzle';
        } elseif (in_array($code, $showers)) {
            $state = 'showers';
        } elseif (in_array($code, $snowFlurries)) {
            $state = 'snow flurries';
        } elseif (in_array($code, $hail)) {
            $state = 'hail';
        } elseif (in_array($code, $foggy)) {
            $state = 'foggy';
        } elseif (in_array($code, $cold)) {
            $state = 'cold';
        } elseif (in_array($code, $cloudy)) {
            $state = 'cloudy';
        } elseif (in_array($code, $mostlyCloudy)) {
            $state = 'mostly cloudy';
        } elseif (in_array($code, $partlyCloudy)) {
            $state = 'partly cloudy';
        } elseif (in_array($code, $clear)) {
            $state = 'clear';
        } elseif (in_array($code, $mixedRainHail)) {
            $state = 'mixed rain and hail';
        } elseif (in_array($code, $scatteredThunderstorms)) {
            $state = 'scattered thunderstorms';
        }
        return $state;
    }
}
