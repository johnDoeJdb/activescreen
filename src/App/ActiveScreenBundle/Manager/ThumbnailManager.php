<?php

namespace App\ActiveScreenBundle\Manager;

class ThumbnailManager
{
    public function createThumbnail($videoPath, $thumbnailPath, $interval = 2)
    {
        $ffmpeg = '/usr/bin/ffmpeg';
        preg_match('/([\w\d\v\s]*)\.[\w\d]{3,4}/', '.'.$videoPath, $name);
        $thumbnail = $thumbnailPath.'/'.$name[1].'.jpg';
        $size = '260x200';
        $cmd = "$ffmpeg -i .$videoPath -deinterlace -an -ss $interval -f mjpeg -t 1 -r 1 -y -s $size .$thumbnail 2>&1";
        exec($cmd);

        return $thumbnail;
    }
}
