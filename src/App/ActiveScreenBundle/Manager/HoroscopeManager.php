<?php

namespace App\ActiveScreenBundle\Manager;

use App\ActiveScreenBundle\Entity\Horoscope;
use Symfony\Component\DependencyInjection\Container;

class HoroscopeManager
{
    const HOST = 'http://www.findyourfate.com/rss/dailyhoroscope-feed.asp';

    private $container;

    function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getHoroscope()
    {
        $horoscope = $this->getTodayHoroscopeFromDatabase();
        if (!$horoscope = $this->getTodayHoroscopeFromDatabase()) {
            $this->updateHoroscope();
            $horoscope = $this->getTodayHoroscopeFromDatabase();
        }

        $signs = $this->convertToArray($horoscope);

        return $signs;
    }

    private function convertToArray($horoscope)
    {
        $signs = [];
        foreach ($horoscope as $sign) {
            $signs[$sign->getSign()] = $sign->getDescription();
        }

        return $signs;
    }

    private function updateHoroscope()
    {
        if (!$this->checkExistTodayHoroscopeFromDatabase()) {
            $horoscope = $this->getHoroscopeFromRemote();
        }
        $horoscope = $this->getHoroscopeFromRemote();
        $this->saveToDatabase($horoscope);
    }

    private function getHoroscopeFromRemote()
    {
        $signs = ['Capricorn','Aquarius','Pisces','Aries','Taurus','Gemini','Cancer','Leo','Virgo','Libra','Scorpio','Sagittarius'];
        $horoscope = [];
        foreach ($signs as $sign) {
            $url = $this::HOST.'?sign='.$sign;
            if ($content = @file_get_contents($url)) {
                $element = new \SimpleXMLElement($content);
                $horoscope[$sign] = (string) $element->channel->item->description;
            }
        }

        return $horoscope;
    }

    private function saveToDatabase($horoscope) {
        $saveToDatabase = function ($horoscope) {
            foreach ($horoscope as $sign => $description) {
                $entityManager = $this->container->get('doctrine.orm.entity_manager');
                $horocscopeEntity = new Horoscope();
                $horocscopeEntity->setDate(new \DateTime());
                $horocscopeEntity->setSign($sign);
                $horocscopeEntity->setDescription($description);
                $entityManager->persist($horocscopeEntity);
                $entityManager->flush();
            }
        };
        if (!$this->checkExistTodayHoroscopeFromDatabase()) {
            $saveToDatabase($horoscope);
        }
    }

    private function getTodayHoroscopeFromDatabase() {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        if ($horoscope = $entityManager->getRepository('ActiveScreenBundle:Horoscope')->findBy(array('date' => new \DateTime()))) {
            return $horoscope;
        }
    }

    private function checkExistTodayHoroscopeFromDatabase() {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        if ($horoscope = $entityManager->getRepository('ActiveScreenBundle:Horoscope')->findOneBy(array('date' => new \DateTime()))) {
            return true;
        }
    }
}
