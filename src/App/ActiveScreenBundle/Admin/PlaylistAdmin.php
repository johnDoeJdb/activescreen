<?php

namespace App\ActiveScreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PlaylistAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('item', 'sonata_type_collection', array(), array(
                'edit' => 'inline',
                'inline' => 'table',
            ));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
//            ->add('item')
            ->add('user')
            ->add('_action', 'actions', array(
                    'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ))
            )
        ;
    }

    public function prePersist($object)
    {
        foreach ($object->getItem() as $item) {
            $item->setPlaylist($object);
        }
    }

    public function preUpdate($object)
    {
        foreach ($object->getItem() as $item) {
            $item->setPlaylist($object);
        }
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'ActiveScreenBundle:Admin/Playlist:edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}