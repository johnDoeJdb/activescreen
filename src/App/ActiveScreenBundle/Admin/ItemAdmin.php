<?php

namespace App\ActiveScreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ItemAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('video')
            ->add('horoscope')
            ->add('weather')
            ->add('position')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('video')
            ->add('horoscope')
            ->add('weather')
            ->add('position')
            ->add('_action', 'actions', array(
                    'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ))
            )
        ;
    }
}