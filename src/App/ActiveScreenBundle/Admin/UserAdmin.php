<?php

namespace App\ActiveScreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class UserAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username')
            ->add('email')
            ->add('firstName')
            ->add('lastName')
            ->add('phone')
            ->add('country')
            ->add('password')
            ->add('playlist')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('username')
            ->add('email')
            ->add('firstName')
            ->add('lastName')
            ->add('phone')
            ->add('country')
            ->add('playlist')

            ->add('_action', 'actions', array(
                    'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ))
            )
        ;
    }
}