<?php

namespace App\ActiveScreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Filesystem\Filesystem;

class VideoAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('uploadFile', 'file')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('file', null, array('template' => 'ActiveScreenBundle:Admin/Video:row_video.html.twig'))
            ->add('size', null, array('template' => 'ActiveScreenBundle:Admin/Video:row_size.html.twig'))
            ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    ))
            )
        ;
    }

    public function prePersist($video) {
        $this->manageFileUpload($video);
    }

    public function preUpdate($video) {
        $this->manageFileUpload($video);
    }

    private function manageFileUpload($video) {
        if ($uploadFile = $video->getUploadFile()) {
            @mkdir('.'.$video::THUMBNAIL_FOLDER);
            @mkdir('.'.$video::VIDEO_FOLDER);
            $filename = $uploadFile->getClientOriginalName();
            $video->upload();
            $container = $this->getConfigurationPool()->getContainer();
            $thumbnailManager = $container->get('active_screen.thumbnail');
            $thumbnail = $thumbnailManager->createThumbnail($video::VIDEO_FOLDER.'/'.$filename, $video::THUMBNAIL_FOLDER);
            $video->setThumbnail($thumbnail);
        }
    }
}