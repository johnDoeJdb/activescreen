<?php

namespace App\ActiveScreenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\ActiveScreenBundle\Entity\Template as smartScreenTemplate;

class EcpAjaxController extends Controller
{
    /**
     * @Route("/get/rules/by/type", name="ajax_rules_by_type", options={"expose"=true})
     */
    public function getAjaxValidRulesByTypeTemplate()
    {
        $typeTemplate =  $this->get('request')->query->get('typeTemplate');
        $typeForm =  $this->get('request')->query->get('typeForm');
        $json = file_get_contents('templates/template-'.$typeTemplate.'.json');

        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/user/ajax/tabs", name="ajax_get_tabs", options={"expose"=true})
     */
    public function getAjaxTabsForm()
    {
        $blocks =  $this->get('request')->query->get('blocks');
        $tabs = '';
        for ($i=0; $i < $blocks; $i++) {
            $id = $i+1;
            if ($id == 1) {
                $class = 'active';
            } else {
                $class = 'incompleted';
            }
            $tabs.= "<li class=\"".$class."\"><a href=\"#\">{$id}</a></li>";
        }

        return Response::create($tabs);
    }

    /**
     * @Route("/user/ajax/upload/{type}/{id}", name="ajax_upload_template", options={"expose"=true})
     */
    public function uploadTemplate($type, $id)
    {
        $data = $this->get("request")->getContent();
        $index = $this->get("request")->query->get('index');
        $typeTemplate = $this->get("request")->query->get('typeTemplate');

        $catalog = '/media/templates/'.$id;
        @mkdir('.'.$catalog);
        $path = $catalog.'/picture-'.$index.'.png';
        file_put_contents('.'.$path, $data);
        if (!$template = $this->getDoctrine()->getRepository('ActiveScreenBundle:Template')->find($id)) {
            $template = new smartScreenTemplate();
        }
        if ($type == "image") {
            $images = $template->getImage();
            $images[] = $path;
            $template->setImage($images);
        } else {
            $content = json_decode($data);
            $text = $content->text;
            $title = $content->title;
            $templateName = $content->templateName;
            $template->setText($text);
            $template->setTitle($title);
            $template->setName($templateName);
        }
        $user = $this->get('security.context')->getToken()->getUser();
        $template->setUser($user);
        $template->setType($typeTemplate);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($template);
        $entityManager->flush();

        return new Response('ok');
    }

    /**
     * @Route("/user/ajax/get/unique/id", name="ajax_get_unique_id", options={"expose"=true})
     */
    public function getUniqueIdTemplate()
    {
        $id = $this->getDoctrine()->getRepository('ActiveScreenBundle:Template')->getLastId()+1;

        return new Response($id);
    }

    /**
     * @Route("/user/ajax/templates/save", name="save_templates", options={"expose"=true})
     */
    public function templatesSave()
    {
        $tempaltes = json_decode($this->get('request')->getContent());
        $user =  $this->get('security.context')->getToken()->getUser();
        $this->getDoctrine()->getRepository('ActiveScreenBundle:Template')->setEnabled($tempaltes, $user);

        return new Response('ok');
    }

//    /**
//     * @Route("/user/ajax/get//id", name="ajax_get_unique_id", options={"expose"=true})
//     */
//    public function ()
//    {
//        $id = $this->getDoctrine()->getRepository('ActiveScreenBundle:Template')->getLastId()+1;
//
//        return new Response($id);
//    }
}
