<?php

namespace App\ActiveScreenBundle\Controller;

use App\ActiveScreenBundle\Entity\Item;
use App\ActiveScreenBundle\Entity\Playlist;
use App\ActiveScreenBundle\Entity\User;
use App\ActiveScreenBundle\Entity\Video;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AdminAjaxController extends Controller
{
    /**
     * @Route("/admin/ajax/playlist/{id}/video", name="playlist_video", options={"expose"=true})
     */
    public function getPlaylistVideoAction($id)
    {
        $items = $this->getDoctrine()->getRepository('ActiveScreenBundle:Playlist')->fetchItems($id);
        $list = array();
        foreach ($items as $item) {
            $element =array(
                'id' => $item['id'],
                'video_id' => $item['video_id'],
                'position' => $item['position'],
            );
            if ($item['video_file']) {
                preg_match('/([\w\d\v\s\[\]]*)\.[\w\d]{3,4}/', $item['video_file'], $name);
                $element['video_name'] = $name[0];
            }
            if ($item['weather']) {
                $element['video_name'] = 'Weather';
            } elseif ($item['horoscope']) {
                $element['video_name'] = 'Horoscope';
            } else {
                $element['source'] = $this->getRequest()->getSchemeAndHttpHost().$item['video_file'];
            }

            $list[] = $element;
        }

        return JsonResponse::create($list);
    }

    /**
     * @Route("/admin/ajax/playlist/{id}/users", name="playlist_users", options={"expose"=true})
     */
    public function getPlaylistUsersAction($id)
    {
        $playlist = $this->getDoctrine()->getRepository('ActiveScreenBundle:Playlist')->find($id);

        $users = array();
        foreach ($playlist->getUser() as $user) {
            $users[] = array(
                'id' => $user->getId(),
                'username' => $user->getUsername(),
            );
        }

        return JsonResponse::create($users);
    }


    /**
     * @Route("/admin/ajax/playlist/{id}/resort", name="ajax_position_update", options={"expose"=true})
     */
    public function resortPositionsInPlaylist($id)
    {
        $playlist = $this->getDoctrine()->getRepository('ActiveScreenBundle:Playlist')->find($id);
        $items = json_decode($this->get("request")->getContent());

        foreach ($playlist->getItem() as $item) {
            foreach ($items as $gettingItem) {
                if ($gettingItem->id == $item->getId()) {
                    $item->setPosition($gettingItem->position);
                }
            }
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($playlist);
        $entityManager->flush();

        return new Response('ok');
    }

    /**
     * @Route("/admin/ajax/playlist/add/{name}", name="ajax_playlist_add", options={"expose"=true})
     */
    public function addPlaylist($name)
    {
        $playlist = new Playlist();
        $playlist->setName($name);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($playlist);
        $entityManager->flush();

        $item = "<div class=\"item\" id=\"".$playlist->getId()."\">".$playlist->getName()."'</div>"."<span class=\"remove glyphicon glyphicon-trash\"></span>";

        return new Response($item);
    }

    /**
     * @Route("/admin/ajax/upload/video", name="ajax_upload_video", options={"expose"=true})
     */
    public function uploadVideo()
    {
        $file = $this->get("request")->getContent();
        $name = $this->get("request")->query->get('name');
        $mediaPath = '/media/video';
        $path = $mediaPath.'/'.$name;
        $filesystem = new Filesystem();
        $filesystem->mkdir('.'.$mediaPath);
        $filesystem->touch('.'.$path);
        file_put_contents('.'.$path, $file);
        $thumbnailManager = $this->get('active_screen.thumbnail');
        $thumbnail = $thumbnailManager->createThumbnail($path);
        $video = new Video();
        $video->setName($name);
        $video->setFile($path);
        $video->setThumnail($thumbnail);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($video);
        $entityManager->flush();

        return new Response('ok');
    }

    /**
     * @Route("/admin/ajax/list/video", name="ajax_list_video", options={"expose"=true})
     */
    public function listVideo()
    {
        $video = $this->getDoctrine()->getRepository('ActiveScreenBundle:Video')->findAll();
        $items = array();
        foreach ($video as $item) {
            $items[] = '<option id='.$item->getId().'>'.$item->getFile().'</option>'."<span class=\"remove glyphicon glyphicon-trash\"></span>";
        }
        $html = implode('', $items);

        return new Response($html);
    }

    /**
     * @Route("/admin/ajax/assign/video", name="assign_video_to_playlist", options={"expose"=true})
     */
    public function assignVideoToPlaylist()
    {
        $videoId = $this->get('request')->query->get('video');
        $playlistId = $this->get('request')->query->get('playlist');
        $playlist = $this->getDoctrine()->getRepository('ActiveScreenBundle:Playlist')->find($playlistId);
        $item = new \App\ActiveScreenBundle\Entity\Item();
        if (is_numeric($videoId)) {
            $video = $this->getDoctrine()->getRepository('ActiveScreenBundle:Video')->find($videoId);
            $item->setVideo($video);
            $item->setPlaylist($playlist);
            $item->setPosition(99);
        } else {
            $method = ucwords($videoId);
            $item->set{$method}(true);
            $item->setPlaylist($playlist);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($item);
        $entityManager->flush();

        $item = "<div class=\"item\" id=\"{$item->getId()}\">{$item->getVideo()->getFile()}</div>"."<span class=\"remove glyphicon glyphicon-trash\"></span>";

        return new Response($item);
    }

    /**
     * @Route("/admin/ajax/list/users", name="ajax_list_users", options={"expose"=true})
     */
    public function listUsers()
    {
        $users = $this->getDoctrine()->getRepository('ActiveScreenBundle:User')->fetchAvailableUsers();

        $items = array();
        foreach ($users as $user) {
            $items[] = '<option id='.$user->getId().'>'.$user->getUsername().'</option>'."<span class=\"remove glyphicon glyphicon-trash\"></span>";
        }
        $html = implode('', $items);

        return new Response($html);
    }


    /**
     * @Route("/admin/ajax/assign/users", name="assign_users_to_playlist", options={"expose"=true})
     */
    public function assignUsersToPlaylist()
    {
        $userId = $this->get('request')->query->get('user');
        $playlistId = $this->get('request')->query->get('playlist');
        $user = $this->getDoctrine()->getRepository('ActiveScreenBundle:User')->find($userId);

        $item = '';
        if (!$user->getPlaylist()) {
            $playlist = $this->getDoctrine()->getRepository('ActiveScreenBundle:Playlist')->find($playlistId);
            $user->setPlaylist($playlist);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $item = "<div id=\"{$user->getId()}\" class=\"item\">{$user->getUsername()}</div>"."<span class=\"remove glyphicon glyphicon-trash\"></span>";
        }

        return new Response($item);
    }

    /**
     * @Route("/admin/ajax/assign/weather", name="assign_weather", options={"expose"=true})
     */
    public function assignWeatherToPlaylist()
    {
        $playlistId = $this->get('request')->request->get('id');
        $playlist = $this->getDoctrine()->getRepository('ActiveScreenBundle:Playlist')->find($playlistId);
        $item = new Item();
        $item->setWeather(true);
        $item->setPosition(99);
        $item->setPlaylist($playlist);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($item);
        $entityManager->persist($playlist);
        $entityManager->flush();

        $item = "<div id=\"{$item->getId()}\" class=\"item\">Weather</div>"."<span class=\"remove glyphicon glyphicon-trash\"></span>";

        return new Response($item);
    }

    /**
     * @Route("/admin/ajax/assign/horoscope", name="assign_horoscope", options={"expose"=true})
     */
    public function assignHoroscopeToPlaylist()
    {
        $playlistId = $this->get('request')->request->get('id');
        $playlist = $this->getDoctrine()->getRepository('ActiveScreenBundle:Playlist')->find($playlistId);
        $item = new Item();
        $item->setHoroscope(true);
        $item->setPosition(99);
        $item->setPlaylist($playlist);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($item);
        $entityManager->persist($playlist);
        $entityManager->flush();

        $item = "<div id=\"{$item->getId()}\" class=\"item\">Horoscope</div>"."<span class=\"remove glyphicon glyphicon-trash\"></span>";

        return new Response($item);
    }


    /**
     * @Route("/admin/ajax/user/add", name="ajax_add_user", options={"expose"=true})
     */
    public function addUser()
    {
        $params = $this->get('request')->request->all();
        $user = new User();
        $user->setUsername($params['username']);
        $user->setFirstName($params['firstname']);
        $user->setLastName($params['lastname']);
        $user->setEmail($params['email']);
        $user->setPassword($params['password']);
        $user->setEnabled(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return new Response('ok');
    }

    /**
     * @Route("/admin/ajax/playlist/remove", name="remove_playlist", options={"expose"=true})
     */
    public function removePlaylist()
    {
        $playlistId = $this->get('request')->request->get('id');
        $playlist = $this->getDoctrine()->getRepository('ActiveScreenBundle:Playlist')->find($playlistId);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($playlist);
        $entityManager->flush();

        return new Response('ok');
    }

    /**
     * @Route("/admin/ajax/video/remove", name="remove_video", options={"expose"=true})
     */
    public function removeItem()
    {
        $itemId = $this->get('request')->request->get('id');
        $item = $this->getDoctrine()->getRepository('ActiveScreenBundle:Item')->find($itemId);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($item);
        $entityManager->flush();

        return new Response('ok');
    }

    /**
     * @Route("/admin/ajax/user/remove", name="remove_user", options={"expose"=true})
     */
    public function removeUser()
    {
        $userId = $this->get('request')->request->get('id');
        $user = $this->getDoctrine()->getRepository('ActiveScreenBundle:User')->find($userId);
        $user->setPlaylist();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return new Response('ok');
    }
}
