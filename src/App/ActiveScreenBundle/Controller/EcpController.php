<?php

namespace App\ActiveScreenBundle\Controller;

use App\ActiveScreenBundle\Form\ImageType;
use App\ActiveScreenBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class EcpController extends Controller
{
    /**
     * @Route("/user/playlist", name="playlist")
     * @Template()
     */
    public function playlistAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $items = $this->getDoctrine()->getRepository('ActiveScreenBundle:User')->fetchItemsOrderByPosition($user->getId());

        if (!$items) {
            $this->get('session')->getFlashBag()->add('notice', 'Current user does not have a playlist');

            return $this->redirect($this->generateUrl('login'));
        }
        $items = $this->replaceNames($items);

        return array('user' => $user, 'items' => $items);
    }

    /**
     * @Route("/user/templates", name="template")
     * @Template()
     */
    public function templateAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $templates = $this->getDoctrine()->getRepository('ActiveScreenBundle:Template')->fetchTemaplatesByUser($user->getId());

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $templates,
            $this->get('request')->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return array('templates' => $pagination);
    }


    /**
     * @Route("/user/templates/add", name="add_template", options={"expose"=true})
     * @Template()
     */
    public function addTemplateAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
//        $templates = $this->getDoctrine()->getRepository('ActiveScreenBundle:Template')->fetchTemaplatesByUser($user->getId());

        return array();
    }

    /**
     * @Route("/user/template/delete/{id}", name="delete_template", options={"expose"=true})
     */
    public function deleteTemplateAction($id)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $template = $this->getDoctrine()->getRepository('ActiveScreenBundle:Template')->findOneBy(array(
            'id' => $id,
            'user' => $user,
        ));
        $em = $this->getDoctrine()->getManager();
        $em->remove($template);
        $em->flush();

        return $this->redirect($this->generateUrl('template'));
    }


    private function replaceNames($items)
    {
        $replacedItems = array();
        foreach ($items as $item) {
            if ($file = $item['file']) {
                preg_match('/[\w\d\v\s]*\.[\w\d]{3,4}/', $item['file'], $name);
                $item['file'] = $name[0];
                $replacedItems[] = $item;
            }
            continue;
        }

        return $replacedItems;
    }
}
