<?php

namespace App\ActiveScreenBundle\Controller;

use App\ActiveScreenBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     * @Template()
     */
    public function frontAction()
    {
        $usersQueryBuilder = $this->getDoctrine()->getRepository('ActiveScreenBundle:User')->fetchAll();

        $paginator  = $this->get('knp_paginator');
        $users = $paginator->paginate(
            $usersQueryBuilder,
            $this->get('request')->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return array('users' => $users);
    }

//    /**
//     * @Route("/admin/users", name="users")
//     */
//    public function usersAction()
//    {
//
//        return array('fsdf' => 'asd');
//    }

    /**
     * @Route("/admin/content", name="content")
     * @Template()
     */
    public function contentAction()
    {
        $users = $this->getDoctrine()->getRepository('ActiveScreenBundle:User')->findAll();

        return array('users' => $users);
    }

    /**
     * @Route("/admin/playlists", name="playlists")
     * @Template()
     */
    public function playlistsAction()
    {
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('login'));
        }
        $playlists = $this->getDoctrine()->getRepository('ActiveScreenBundle:Playlist')->findAll();
        $form = $this->createForm(new UserType());

        return array('playlists' => $playlists, 'user' => $form->createView());
    }

    /**
     * @Route("/user/playlist", name="playlist")
     * @Template()
     */
    public function playlistAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $items = $this->getDoctrine()->getRepository('ActiveScreenBundle:User')->fetchItemsOrderByPosition($user->getId());

        if (!$items) {
            $this->get('session')->getFlashBag()->add('notice', 'Current user does not have a playlist');

            return $this->redirect($this->generateUrl('login'));
        }
        $items = $this->replaceNames($items);

        return array('user' => $user, 'items' => $items);
    }

    private function replaceNames($items)
    {
        $replacedItems = array();
        foreach ($items as $item) {
            if ($file = $item['file']) {
                preg_match('/\/((\w|\d|\+|_|\-|)*\..*)/', $item['file'], $name);
                $item['file'] = $name[1];
                $replacedItems[] = $item;
            }
            continue;
        }

        return $replacedItems;
    }
}
