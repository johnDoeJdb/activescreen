<?php

namespace App\ActiveScreenBundle\Controller;

use App\ActiveScreenBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class FrontController extends Controller
{
    /**
     * @Route("/", name="front")
     */
    public function frontAction()
    {
        return $this->redirect('/login');
    }


    /**
     * @Route("/redirect", name="redirect")
     */
    public function redirectAction()
    {
        $roles = $this->get('security.context')->getToken()->getRoles();
        foreach ($roles as $role) {
            $items[] = $role->getRole();
        }

        if (in_array('ROLE_ADMIN', $items)) {
            $url = $this->generateUrl('playlists');
        } else {
            $url = $this->generateUrl('playlist');
        }

        return $this->redirect($url);
    }

}
