<?php

namespace App\ActiveScreenBundle\Controller;

use App\ActiveScreenBundle\Entity\Item;
use App\ActiveScreenBundle\Entity\Playlist;
use App\ActiveScreenBundle\Entity\User;
use App\ActiveScreenBundle\Entity\Video;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\SimpleXMLElement;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**
     * @Route("/api", name="json_data_by_user", options={"expose"=true})
     */
    public function getData()
    {
        $authenticationManager = $this->get('active_screen.authentication_manager');
        $query = $this->get('request')->query;
        $user = $query->get('user');
        $user = $this->container->get('doctrine.orm.entity_manager')->getRepository('ActiveScreenBundle:User')->findOneByUsername($user);
        $password = $query->get('password');
        $longitude = $query->get('longitude');
        $latitude = $query->get('latitude');
        $credentials = $authenticationManager->checkUserCredentials($user, $password);
        if (!$credentials) {
            return $authenticationManager->getResponseError();
        }
        $templates = $this->getDoctrine()->getRepository('ActiveScreenBundle:Template')->findBy(array('user' => $user, 'enabled' => true));
        $items = $this->getDoctrine()->getRepository('ActiveScreenBundle:Item')->getSortedPlaylist($user);
        $weather = $this->get('active_screen.weather')->getForecast($latitude, $longitude);
        $horoscope = $this->get('active_screen.horoscope')->getHoroscope();
        $listItems = [];
        $listTemplates = [];
        $playlist = [];
        foreach ($items as $item) {
            $element = [];
            if ($video = $item->getVideo()) {
                preg_match('/[\w\d]{3,4}$/', $video->getFile(), $format);
                preg_match('/([\w\d\v\s]*)\.[\w\d]{3,4}/', $video->getFile(), $name);
                $element['type'] = 0;
                $element['name'] = $name[1];
                $element['format'] = $format[0];
                $element['url'] = $this->getRequest()->getSchemeAndHttpHost().$video->getFile();
                $element['size'] = ceil((filesize('.'.$video->getFile())/1048576)).'Mb';
            } elseif ($item->getWeather()) {
                $element['type'] = 1;
                $element['data'] = $weather;
            } elseif ($item->getHoroscope()) {
                $element['type'] = 2;
                $element['data'] = $horoscope;
            }
            $listItems[] = $element;
        }
        foreach ($templates as $template) {
            if (!$a = $template->getPosition() == 'Last') {
                $sorterTemplates[0] = $template;
            } else {
                $sorterTemplates[1] = $template;
            }
        }
        foreach ($sorterTemplates as $template) {
            $templateType = json_decode(file_get_contents('templates/template-'.$template->getType().'.json'));
            $numberElements = $templateType->text;
            $element = [];
            $element['type'] = $template->getType();
            for ($i = 0; $i < $numberElements; $i++) {
                $block = $i+1;
                $element['text'][] = array(
                    'block' => $block,
                    'title' => $template->getTitle(),
                    'content' => $template->getText(),
                    'company' => $user->getCompanyName(),
                );
                $element['image'][] = array(
                    'block' => $block,
                    'url' => $this->getRequest()->getSchemeAndHttpHost().$template->getImage()[$i],
                );
            }
            $listTemplates[] = $element;
        }
        if ($template = $item->getVideo()) {
            $element['type'] = 0;
            $element['name'] = 0;
            $element['format'] = 0;
            $element['url'] = $this->getRequest()->getSchemeAndHttpHost().$video->getFile();
        } elseif ($item->getWeather()) {
            $element['type'] = 1;
            $element['data'] = $weather;
        } elseif ($item->getHoroscope()) {
            $element['type'] = 2;
            $element['data'] = $horoscope;
        }
        $response['playlist'] = $listItems;
        $response['templates'] = $listTemplates;

        return JsonResponse::create($response);
    }
}
