<?php

namespace App\ActiveScreenBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function fetchAvailableUsers()
    {
        $qb = $this->createQueryBuilder('u')
            ->andWhere('u.playlist is NULL');

        return $qb->getQuery()->getResult();
    }

    public function fetchItemsOrderByPosition($id)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('i.id', 'i.position', 'i.weather', 'i.horoscope', 'v.id', 'v.file')
            ->where('u.id = :id')
            ->leftJoin('u.playlist', 'p')
            ->leftJoin('p.item', 'i')
            ->leftJoin('i.video', 'v')
            ->orderBy('i.position', 'ASC')

            ->setParameter('id', $id);

        return $qb->getQuery()->getResult();
    }

    public function fetchAll()
    {
        $qb = $this->createQueryBuilder('u');


        return $qb;
    }
}