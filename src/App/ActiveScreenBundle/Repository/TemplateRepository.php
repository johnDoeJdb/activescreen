<?php

namespace App\ActiveScreenBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TemplateRepository extends EntityRepository
{
    public function getLastId()
    {
        $qb = $this->createQueryBuilder('t')
            ->select('t.id')
            ->orderBy('t.id', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function fetchTemaplatesByUser($id)
    {
        $qb = $this->createQueryBuilder('t')
            ->leftJoin('t.user', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getResult();
    }

    public function deleteTemaplateByUser($id)
    {
        $qb = $this->createQueryBuilder('t')
            ->leftJoin('t.user', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getResult();
    }

    public function findByIds($ids)
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', $ids);

        return $qb->getQuery()->getResult();
    }

    public function setEnabled($templates, $user) {
        $ids = $this->createQueryBuilder('t')
            ->select('t.id')
            ->join('t.user', 'u')
            ->where('u.id = :id')
            ->set('t.enabled', 'false')
            ->setParameter('id', $user->getId())
            ->getQuery()
            ->getScalarResult();

        foreach ($ids as $id) {
            $allId[] = $id['id'];
        }

        $this->createQueryBuilder('t')
            ->update()
            ->where('t.id IN (:ids)')
            ->set('t.enabled', 'false')
            ->setParameter('ids', $allId)
            ->getQuery()
            ->getResult();

        foreach ($templates as $template) {
            $id = $template->id;
            $position = $template->type;

            $this->createQueryBuilder('t')
                ->update()
                ->where('t.id = :id')
                ->set('t.enabled', 'true')
                ->set('t.position', ':position')
                ->setParameter('id', $id)
                ->setParameter('position', $position)
                ->getQuery()
                ->getResult();
        }
    }
}