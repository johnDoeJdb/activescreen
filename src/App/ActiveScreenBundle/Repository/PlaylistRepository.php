<?php

namespace App\ActiveScreenBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PlaylistRepository extends EntityRepository
{
    public function fetchAvailableUsers($id)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('u.id, u.username, u.playlist ')
            ->leftJoin('p.user', 'u')
            ->leftJoin('u.playlist', 'u')
            ->where('p.id = :id')
//            ->andWhere('u.playlist is NULL')
            ->setParameter('id', $id);

        return $qb->getQuery()->getResult();
    }


    public function fetchItems($id)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('i.id, v.file video_file, v.id as video_id , i.position, i.weather, i.horoscope', 'i.template')
            ->leftJoin('p.item', 'i')
            ->leftJoin('i.video', 'v')
            ->where('p.id = :id')
            ->andWhere('i.playlist is not NULL')
            ->setParameter('id', $id)
            ->orderBy('i.position', 'ASC');

        return $qb->getQuery()->getResult();
    }
}